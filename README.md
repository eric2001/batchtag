# BatchTag
![BatchTag](./screenshot.jpg) 

## Description
BatchTag is a module for Gallery 3 which will allow gallery users to automatically apply a tag to the entire contents of an album. BatchTag works by creating an additional input box in the album sidebar where tags may be entered (multiple tags should be seperated by a comma). The tags will then automatically be assigned to every non-album in that album. An optional checkbox allows for the contents of sub-albums to be tagged as well.  BatchTag requires that the Tag module be installed and active in order to function properly.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/101076).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "batchtag" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 1.5.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 24 August 2021.
>
> Download: [Version 1.5.0](/uploads/724d0a3686d49c43237b9f912b3ea910/batchtag150.zip)

**Version 1.4.1:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 15 August 2011.
>
> Download: [Version 1.4.1](/uploads/95bae63c61e56eeea27c3be2c824fbcc/batchtag141.zip)

**Version 1.4.0:**
> - Updated to better handle tagging really large quantities of photos.
> - Updated "tags module required" admin notification code to current standards.
> - Integrated the Tag modules auto-complete feature into the batchtag sidebar.
> - Released 02 March 2011.
>
> Download: [Version 1.4.0](/uploads/09d8e258637e49143c7a88c05445681b/batchtag140.zip)

**Version 1.3.1:**
> - Minor bugfix for RC-1 compatibility.
> - Released 24 February 2010.
>
> Download: [Version 1.3.1](/uploads/a801bfd8c5a15efe836736a20e1bd9f3/batchtag131.zip)

**Version 1.3.0:**
> - Updated for recent API changes in Gallery 3.
> - Now displays a warning message if the Tags module is not active.
> - Fixed a bug that causes it to crash when tagging the contents of sub-albums.
> - Released 20 January 2010.
>
> Download: [Version 1.3.0](/uploads/4c77f4b65278ec7ff5270d00220db9a6/batchtag130.zip)

**Version 1.2.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Updated for the new sidebar code in Gallery 3 Git
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 12 October 2009
>
> Download: [Version 1.2.0](/uploads/3bf652acacf28cec4cc84f2616eddae5/batchtag120.zip)

**Version 1.1.0:**
> - Added an option for including the contents of all sub-albums when tagging.
> - Released 30 September 2009
>
> Download: [Version 1.1.0](/uploads/7f0533bc96b330d8ecc12f911a3dada3/batchtag110.zip)

**Version 1.0.1:**
> - Minor bug fix.
> - Released 20 August 2009
>
> Download: [Version 1.0.1](/uploads/715d5d3a59f5b5b9fe95451121173317/batchtag101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 04 August 2009
>
> Download: [Version 1.0.0](/uploads/f6e0e738bfb716278d3f2240972912f8/batchtag100.zip)
